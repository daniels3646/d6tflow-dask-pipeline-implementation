import sys
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask
from dskaggle.viz_util import prttPrnt,basic_info,basic_info_df,basic_info_ser,extensive_info
from fe import *
from modeling import *
from read_from_csv import *
from viz import *
import argparse
import inspect
import shutil

if __name__ == '__main__':
  d6tflow.settings.log_level = 'WARNING' # 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'

  parser = argparse.ArgumentParser()
  parser.add_argument('-t', action='store', help='the task to run')
  parser.add_argument('-i', nargs='?', const='all', help='delimited list input', type=str)
  parser.add_argument('-f', action='store_true', help= 'invalidate all upstream')
  parser.add_argument('-D', action='store_true', help= 'invalidate specific task and run')
  parser.add_argument('-d', action='store_true', help= 'invalidate specific task')
  parser.add_argument('-b', action='store_true', help='save output df as html file')
  parser.add_argument('-e', action='store_true', help='run pandas profiling')
  parser.add_argument('-s', action='store_true', help='see outputs of task')
  args = parser.parse_args()

  start_time = time.time()

  task = locals()[args.t]
  html = True if args.b else False
  if args.s:
    d6tflow.run(viz_util.see_outputs(task))
  if args.d:
    shutil.rmtree('./data/'+ task().__class__.__name__, ignore_errors=True)
    task().invalidate(confirm=False)
  else:
    if args.D:
      shutil.rmtree('./data/'+ task().__class__.__name__, ignore_errors=True)
      task().invalidate(confirm=False)
    if args.f:
      list(map(lambda x: shutil.rmtree('./data/'+ x.__class__.__name__, ignore_errors=True) ,d6tflow.taskflow_upstream(task())))
    d6tflow.preview(task())
    if args.i:
      if args.i == 'all':
        d6tflow.run(basic_info(task,html=html))
      else:
        my_list = [str(item) for item in args.i.split(',')]
        d6tflow.run(basic_info(task,cols=my_list,html=html))
    elif args.e:
      d6tflow.run(extensive_info(task))
    else:
      d6tflow.run(task())

  elapsed_time = time.time() - start_time
  print("Elapsed time: {}".format(util.hms_string(elapsed_time)))
