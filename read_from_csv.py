import os
import csv
import matplotlib.pyplot as plt
import d6tflow
import luigi
from luigi.util import inherits
import sklearn, sklearn.datasets, sklearn.svm, sklearn.linear_model
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import numpy as np
import dask
import sys
import time
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask

aisles = 'input/aisles.csv'
departments = 'input/departments.csv'
order_products_prior = 'input/order_products_prior.csv'
order_products_train = 'input/order_products_train.csv'
orders = 'input/orders.csv'
products = 'input/products.csv'

project_settings = {
  "PATH": "/home/daniel/Documents/DataScience/kaggleKernels/instacart_market_basket_analysis",
  "DTYPES":{
    "order_id": np.int32,
    "product_id": np.uint16,
    "add_to_cart_order": np.int16,
    "reordered": np.int8,
    "user_id": np.int32,
    "eval_set": np.str,
    "order_number": np.int16,
    "order_dow": np.int8,
    "order_hour_of_day": np.int8,
    "days_since_prior_order": np.float32,
    "aisle_id": np.uint8,
    "aisle": np.str,
    "department": np.str,
    "department_id": np.uint8,
    "product_name": np.str
  }
}

class readAll(d6tflow.tasks.TaskAggregator):
  def run(self):
    yield readAisles()
    yield readDepartments()
    yield readPriors()
    yield readTrain()
    yield readOrders()
    yield readProducts()

class readAisles(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + aisles)
    self.save(df)

class readDepartments(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + departments)
    self.save(df)

class readPriors(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + order_products_prior,dtype=project_settings['DTYPES'])
    self.save(df)

class readTrain(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + order_products_train,dtype=project_settings['DTYPES'])
    self.save(df)

class readOrders(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + orders,dtype=project_settings['DTYPES'])
    self.save(df)

class readProducts(TaskParquetDask):
  def run(self):
    df = dd.read_csv('../' + products,dtype=project_settings['DTYPES'])
    self.save(df)
