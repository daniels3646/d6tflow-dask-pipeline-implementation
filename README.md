# d6tflow + dask data science pipeline implementation

this is an example of a d6tflow data science pipeline with dask

i pulled all the code from this great kaggle kernel - https://www.kaggle.com/rshally/instacart-lb-392-runs-on-kaggle-with-2nd-clf/notebook
and just converted it to work with dask and d6tflow plus a few minor tweaks

with this workflow i find it much easier to go back and forth between eda and fe without needing to recalculate things i dont need.
also, it helps me to keep my code organized and clear.

inspired by:
https://towardsdatascience.com/how-to-use-airflow-style-dags-for-highly-effective-data-science-workflows-b52053dedd32
https://towardsdatascience.com/4-reasons-why-your-machine-learning-code-is-probably-bad-c291752e4953

takes 15 mins to run from start to finish on my desktop with 16 GB RAM Intel i7-6700 CPU 
