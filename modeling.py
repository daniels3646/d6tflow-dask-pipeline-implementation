import os
import csv
import matplotlib.pyplot as plt
import d6tflow
import luigi
from luigi.util import inherits
import sklearn, sklearn.datasets, sklearn.svm, sklearn.linear_model
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import numpy as np
import dask
import sys
import time
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask
import lightgbm as lgb
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics
from dask_ml.model_selection import train_test_split
from fe import *
from read_from_csv import *
from dskaggle.viz_util import basic_info_df,basic_info_ser,extensive_info

def combi(z,df): # this summarizes the predictions into order_id : list(product_id)
    prd_bag = dict()
    z_bag = dict()
    for row in df.itertuples():
      if row.reordered > z:
        try:
          prd_bag[row.order_id] += ' ' + str(row.product_id)
          z_bag[row.order_id]+= ' ' + str(int(100*row.reordered))
        except:
          prd_bag[row.order_id] = str(row.product_id)
          z_bag[row.order_id]= str(int(100*row.reordered))

    for order in df.order_id:
      if order not in prd_bag:
        prd_bag[order] = ' '
        z_bag[order] = ' '

    return prd_bag,z_bag

# F1 function uses the actual products as a list in the train set and the list of predicted products

def f1_score_single(x):                 #from LiLi but modified to get 1 for both empty

    y_true = x.actual
    y_pred = x.list_prod
    if y_true == '' and y_pred ==[] : return 1.
    y_true = set(y_true)
    y_pred = set(y_pred)
    cross_size = len(y_true & y_pred)
    if cross_size == 0: return 0.
    p = 1. * cross_size / len(y_pred)
    r = 1. * cross_size / len(y_true)
    return 2 * p * r / (p + r)

class TrainEval(TaskParquetDask):
  persist = ['X_train', 'X_eval', 'y_train', 'y_eval']
  def requires(self):
    return mergeWithTrainTest()
  def run(self):
    train = self.input()['train'].load()
    X_train, X_eval, y_train, y_eval = train_test_split(train[train.columns.difference(['reordered'])], train['reordered'], test_size=0.1, random_state=2)
    dct = dict(zip(self.persist,[X_train, X_eval, y_train.to_frame('y_train'), y_eval.to_frame('y_eval')]))
    self.save(dct)
    # viz_util.sanity_check(self.input(),'test')

class Lgb(d6tflow.tasks.TaskPickle):
  def requires(self):
    return TrainEval()
  def run(self):
    print('formatting and training LightGBM ...')
    X_train = self.input()['X_train'].load(pd=True)
    X_eval= self.input()['X_eval'].load(pd=True)
    y_train= self.input()['y_train'].load(pd=True).iloc[:,0]
    y_eval  = self.input()['y_eval'].load(pd=True).iloc[:,0]
    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_eval = lgb.Dataset(X_eval, y_eval, reference = lgb_train)
    params = {'task': 'train', 'boosting_type': 'gbdt',   'objective': 'binary', 'metric': {'binary_logloss', 'auc'},
    'num_iterations' : 1000, 'max_bin' : 100, 'num_leaves': 512, 'feature_fraction': 0.8,  'bagging_fraction': 0.95,
    'bagging_freq': 5, 'min_data_in_leaf' : 200, 'learning_rate' : 0.05}
    # set lower num_boost_round (I used 300 instead of 50 at home) to avoid time-out on Kaggle
    lgb_model = lgb.train(params, lgb_train, num_boost_round = 50, valid_sets = lgb_eval, early_stopping_rounds=10)
    self.save(lgb_model)


class f1Calc(d6tflow.tasks.TaskPickle):
  def requires(self):
    return dict(zip(['actual','trainTestMerge','checkReorder'],[trainTestActual(),mergeWithTrainTest(),checkReorder()]))
  def run(self):
    print(' summarizing products and probabilities ...')
    # get the prediction for a range of thresholds
    tt=self.input()['actual']['traintest'].load()
    n_actual = self.input()['actual']['n_actual'].load()
    checkReorder = self.input()['checkReorder'].load()
    check = self.input()['trainTestMerge']['check'].load(pd=True)
    check['reordered'] = checkReorder
    i=0
    for z in [0.17, 0.21, 0.25]:
        prd_bag,z_bag = combi(z,check)
        ptemp = pd.DataFrame.from_dict(prd_bag, orient='index')
        ptemp.reset_index(inplace=True)
        ztemp = pd.DataFrame.from_dict(z_bag, orient='index')
        ztemp.reset_index(inplace=True)
        ptemp.columns = ['order_id', 'products']
        ztemp.columns = ['order_id', 'zs']
        ptemp['list_prod'] = ptemp['products'].apply(lambda x: list(map(int, x.split())))
        ztemp['list_z'] = ztemp['zs'].apply(lambda x: list(map(int, x.split())))
        n_cart = ptemp['products'].apply(lambda x: len(x.split())).mean()
        tt = tt.merge(ptemp,on='order_id',how='inner')
        tt = tt.merge(ztemp,on='order_id',how='inner')
        tt.drop(['products','zs'],axis=1,inplace=True)
        tt['zavg'] = tt['list_z'].apply(lambda x: 0.01*np.mean(x) if x!=[] else 0.).astype(np.float16)
        tt['zmax'] = tt['list_z'].apply(lambda x: 0.01*np.max(x) if x!=[] else 0.).astype(np.float16)
        tt['zmin'] = tt['list_z'].apply(lambda x: 0.01*np.min(x) if x!=[] else 0.).astype(np.float16)
        tt['f1']=tt.apply(f1_score_single,axis=1).astype(np.float16)
        F1 = tt['f1'].loc[tt['eval_set']==1].mean()
        tt = tt.rename(columns={'list_prod': 'prod'+str(i), 'f1': 'f1'+str(i), 'list_z': 'z'+str(i),
                    'zavg': 'zavg'+str(i), 'zmax': 'zmax'+str(i),  'zmin': 'zmin'+str(i)})
        print(' z,F1,n_actual,n_cart :  ', z,F1,n_actual,n_cart)
        i=i+1
    tt['fm'] = tt[['f10', 'f11', 'f12']].idxmax(axis=1)
    tt['f1'] = tt[['f10', 'f11', 'f12']].max(axis=1)
    tt['fm'] = tt.fm.replace({'f10': 0,'f11': 1, 'f12':2}).astype(np.uint8)
    print(' f1 maximized ', tt['f1'].loc[tt['eval_set']==1].mean())
    self.save(tt)


class trainTestActual(d6tflow.tasks.TaskPickle): # Training and test data for later use in F1 optimization and training
  persist = ['traintest','n_actual']
  def requires(self):
    return dict(zip(['train','orders'],[mergeTrainOrders(),readOrders()]))
  def run(self):
    train_merged = self.input()['train'].load()
    orders = self.input()['orders'].load().set_index('order_id')
    # train_orders = util.select(self.input()['train'],self.input()['train'],None,'reordered',lambda x: x == 1,merged=[True,True],pd=True).drop('reordered',axis=1)
    train_orders = train_merged[train_merged['reordered']==1].drop('reordered',axis=1)
    # orders = df_concat(self.input()['orders'],None,with_pd=True)
    # orders = orders.set_index('order_id')
    train=orders[['eval_set']].loc[orders['eval_set']=='train']
    train['actual'] = train_orders.groupby('order_id').agg({'product_id':lambda x: list(x)})
    train['actual']=train['actual'].fillna('')
    # basic_info_df(train,pd=True)
    n_actual = train['actual'].apply(lambda x: len(x)).mean()   # this is the average cart size
    test=orders[['eval_set']].loc[orders['eval_set']=='test']
    test['actual']=' '
    # basic_info_df(test,pd=True)
    traintest=pd.concat([train,test])
    # basic_info_df(traintest,pd=True)
    dct = dict(zip(self.persist,[traintest,n_actual]))
    self.save(dct)


class checkReorder(d6tflow.tasks.TaskPickle): # predictin col (reordered) for check (train + test)
  def requires(self):
    return dict(zip(['merge','model'],[mergeWithTrainTest(),Lgb()]))
  def run(self):
    check = self.input()['merge']['check'].load(pd=True)
    lgb_model = self.input()['model'].load()
    check_pred = lgb_model.predict(check[check.columns.difference(['order_id', 'product_id'])], num_iteration = lgb_model.best_iteration)
    self.save(check_pred)

class secondClassifier(d6tflow.tasks.TaskCSVPandas): # Fitting the second classifier for F1 ...
  def requires(self):
    return f1Calc()
  def run(self):
    tt = self.input().load()
    X=tt[[ 'zavg0', 'zmax0','zmin0', 'zavg1', 'zmax1', 'zmin1', 'zavg2', 'zmax2', 'zmin2']].loc[tt['eval_set']=='train']
    y=tt['fm'].loc[tt['eval_set']=='train']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

    clf = GradientBoostingClassifier().fit(X_train, y_train)
    print('GB Accuracy on training set: {:.2f}' .format(clf.score(X_train, y_train)))
    print('Accuracy on test set: {:.2f}' .format(clf.score(X_test, y_test)))
    #pd.DataFrame(clf.feature_importances_, index=X_train.columns, columns=["Importance"]).plot(kind='bar')
    #plt.show()

    final=tt[['order_id','prod0','prod1','prod2','zavg0']].loc[tt['eval_set']=='test']
    df_test=tt[[ 'zavg0', 'zmax0','zmin0', 'zavg1', 'zmax1', 'zmin1', 'zavg2', 'zmax2', 'zmin2']].loc[tt['eval_set']=='test']
    final['fit']= clf.predict(df_test)
    final['best'] = final.apply(lambda row: row['prod0'] if row['fit']==0 else
                                     ( row['prod1'] if row['fit']==1 else  row['prod2'] )  , axis=1)
    #final['products']=final['best'].apply(lambda x: ' '.join(str(i) for i in x) if x!=[] else 'None')

    # I am adding 'None' to orders with one or two products because of the bias in F1
    final['products']=final.apply(self.mylist,axis=1)

    self.save(final[['order_id','products']],index=False)

  def mylist(self,x):
    prodids = x.best
    zavg = x.zavg0
    if prodids == []: return 'None'
    if zavg < 0.5:
        if len(prodids) == 1: return  str(prodids[0])+' None'
        if len(prodids) == 2: return  str(prodids[0])+ ' '+ str(prodids[1]) +' None'
    return ' '.join(str(i) for i in prodids)


class featureImp(TaskParquetDask):
  def requires(self):
    return Lgb()
  def run(self):
    lgb.plot_importance(self.input().load(), figsize=(7,9))
    plt.show()

