# feature engineering
import os
import csv
import matplotlib.pyplot as plt
import d6tflow
import luigi
from luigi.util import inherits
import sklearn, sklearn.datasets, sklearn.svm, sklearn.linear_model
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import numpy as np
import dask
import sys
import time
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask
from dskaggle.viz_util import basic_info_df ,basic_info_ser ,concat_outs
from read_from_csv import *
from sklearn.preprocessing import FunctionTransformer
from sklearn.compose import make_column_transformer
from sklearn.pipeline import make_pipeline
from sklearn.pipeline import make_union
import joblib


class mergePriorOrders(TaskParquetDask):
  def requires(self):
    return dict(zip(['prior','orders'],[readPriors(),readOrders()]))
  def run(self):
    prior = self.input()['prior'].load(dask=True)
    orders = self.input()['orders'].load(dask=True)
    result = prior.merge(orders,on='order_id').compute().sort_values(['user_id', 'order_number', 'product_id'], ascending=True)
    self.save(result)

class mergeTrainOrders(TaskParquetDask):
  def requires(self):
    return dict(zip(['train','orders'],[readTrain(),readOrders()]))
  def run(self):
    train = self.input()['train'].load(dask=True)
    orders = self.input()['orders'].load(dask=True,cols=['user_id','order_id'])
    result = train.merge(orders,on='order_id').compute()
    self.save(result)

class mergeWithTrainTest(TaskParquetDask):
  persist = ['train','test','check']
  def requires(self):
    return dict(zip(['usersFeats','orders','upFeats','productFeats','train'],[usersFeats(),readOrders(),userProductFeats(),productFeats(),mergeTrainOrders()]))
  def run(self):
    orders = self.input()['orders'].load(dask=True,cols=['user_id', 'order_id', 'eval_set', 'days_since_prior_order'])
    usersFeats = concat_outs(self.input()['usersFeats'],dask=True)
    upFeats = concat_outs(self.input()['upFeats'],dask=True)
    products = concat_outs(self.input()['productFeats'],dask=True)
    train = self.input()['train'].load(dask=True,cols=['user_id','product_id','reordered'])

    TrainTestSelection = orders[orders['eval_set']!='prior'].set_index('user_id')
    merge_with_users = usersFeats.merge(TrainTestSelection).reset_index() # this makes sense because the order file is sorted by user_id and the train and test both contain one order from different groups of users if you get combine both the train and test orders it will make the whole population of users and each user only once . so the indexes match perfectly with the user features table.
    merge_with_products = upFeats.merge(products,on='product_id')
    data = merge_with_products.merge(merge_with_users,on='user_id')
    data = data.merge(train,on=['user_id','product_id'],how='left').compute()
    data['reordered'].fillna(0,inplace=True)
    train = data[data['eval_set'] == 'train'].drop(['eval_set', 'user_id', 'product_id', 'order_id'], axis = 1)
    test =  data[data['eval_set'] == 'test'].drop(['eval_set', 'user_id','reordered'], axis = 1)
    check = data.drop(['eval_set', 'user_id', 'reordered'], axis = 1)
    dct = dict(zip(self.persist,[train,test,check]))
    self.save(dct)

class userProductFeats(TaskParquetDask):
  persist = ['up_feats']
  def requires(self):
    return mergePriorOrders()
  def run(self):
    mpo = self.input().load(dask=True,cols=['user_id','product_id','order_number','add_to_cart_order'])
    grouped = mpo.groupby(['user_id','product_id'])
    up_orders = FunctionTransformer(lambda x: x.size().to_frame('up_orders').reset_index(),validate=False)
    up_first_order = FunctionTransformer(lambda x: x['order_number'].min().to_frame('up_first_order').reset_index(drop=True),validate=False)
    up_last_order = FunctionTransformer(lambda x: x['order_number'].max().to_frame('up_last_order').reset_index(drop=True),validate=False)
    up_average_cart_position = FunctionTransformer(lambda x: x['add_to_cart_order'].mean().to_frame('add_to_cart_order').reset_index(drop=True),validate=False)
    lst = [
      concat([up_orders,up_first_order,up_last_order,up_average_cart_position],grouped,compute=True),
    ]
    self.save(dict(zip(self.persist,lst)))

def add_ratio(X):
  X['reorder_ratio'] = X['reorders_total'] / X['prod_total']
  return X

class productFeats(TaskParquetDask): # groupby on product_id
  persist = ['prod_reorder_probability','product_id_reorder']
  def requires(self):
    return mergePriorOrders()
  def run(self):
    mpo = self.input().load(dask=True,cols=['user_id','product_id','reordered'])

    productTime = mpo.groupby(['user_id','product_id']).cumcount().rename('productTime')
    selection = mpo[productTime == 0]
    selection2 = mpo[productTime == 1]
    prod_first_order = selection.groupby('product_id').size().rename('prod_first_order')
    prod_second_order = selection2.groupby('product_id').size().rename('prod_second_order')
    prod_reorder_probability = (prod_second_order/prod_first_order).to_frame('prod_reorder_probability')

    grouped_prod_id = mpo[['product_id','reordered']].groupby('product_id')

    prod_total = FunctionTransformer(lambda x: x.size().rename('prod_total').astype(np.int32),validate=False)
    reorders_total = FunctionTransformer(lambda x: x['reordered'].sum().rename('reorders_total').astype(np.float32),validate=False)
    concat_df = concat([prod_total,reorders_total],grouped_prod_id)
    lst = [
      prod_reorder_probability,
      FunctionTransformer(add_ratio,validate=False).fit_transform(concat_df),
    ]
    self.save(dict(zip(self.persist,lst)))

def concat(lst,group,compute=False):
  if compute:
    return dd.concat(list(map(lambda x: x.fit_transform(group),lst)),axis=1).compute()
  else:
    return dd.concat(list(map(lambda x: x.fit_transform(group),lst)),axis=1)

def concat_pd(lst,group):
  return pd.concat(list(map(lambda x: x.fit_transform(group),lst)),axis=1)

class usersFeats(TaskParquetDask):
  persist = ['orders_uid','mergePriorOrders_uid','ratio']
  def requires(self):
    return dict(zip(['mergePriorOrders','allOrders'],[mergePriorOrders(),readOrders()]))
  def run(self):
    po = self.input()['mergePriorOrders'].load(dask=True,cols=['user_id','product_id','reordered','order_number'])
    ao = self.input()['allOrders'].load(dask=True,cols=['user_id','order_number','days_since_prior_order'])
    ###################### groupby on user_id on orders file on priors selections
    eval_set = self.input()['allOrders'].load(dask=True,cols='eval_set')
    priors_selection = ao[eval_set=='prior']
    grouped_prio_sel = priors_selection.groupby('user_id')
    user_orders = FunctionTransformer(lambda x: x['order_number'].max().to_frame('user_orders'),validate=False)
    user_period = FunctionTransformer(lambda x: x['days_since_prior_order'].sum().to_frame('user_period'),validate=False)
    user_mean_days_since_prior = FunctionTransformer(lambda x: x['days_since_prior_order'].mean().to_frame('user_mean_days_since_prior'),validate=False)
    ###################### groupby on user_id on mergePriorOrders file (for the products)
    grouped_user_id = po.groupby('user_id')
    user_total_products = FunctionTransformer(lambda x: x.size().to_frame('user_total_products'),validate=False)
    user_distinct_products = FunctionTransformer(lambda x: x['product_id'].nunique().to_frame('user_distinct_products'),validate=False)
    reorders_selection = po[po['reordered']==1]
    order_number_gt_selection = po[po['order_number'] > 1]
    eq1 = reorders_selection.groupby('user_id').size().to_frame('eq1')
    gt1 = order_number_gt_selection.groupby('user_id').size().to_frame('gt1') # not sure why this works it has less fucking rows
    user_reorder_ratio = (eq1.iloc[:,0] / gt1.iloc[:,0]).to_frame('user_reorder_ratio')
    lst = [
      concat([user_orders,user_period,user_mean_days_since_prior],grouped_prio_sel),
      concat([user_total_products,user_distinct_products],grouped_user_id),
      user_reorder_ratio
    ]
    self.save(dict(zip(self.persist,lst)))
