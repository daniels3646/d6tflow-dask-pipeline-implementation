import os
import csv
import matplotlib.pyplot as plt
import d6tflow
import luigi
from luigi.util import inherits
import sklearn, sklearn.datasets, sklearn.svm, sklearn.linear_model
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import numpy as np
import dask
import sys
import time
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask
from fe import *
from read_from_csv import *

class viz(d6tflow.tasks.TaskData):
  def requires(self):
    # return dict(zip(util.dsts,[util.readTrainCols(),util.readTestCols()]))
    return dict(zip(['prior','orders','products','departments'],[readOrderproductsprior(),readOrders(),readProducts(),readDepartments()]))
    # return readOrders()

  def run(self):
    ######################## orders

    # unlabelenc = dict(zip(list(range(7)),['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri']))
    # unlabeled = util.applymapOnColumns([ self.input()['order_dow'].load() ],['unlabeled'],lambda x: unlabelenc[x])
    # viz_util.freq(unlabeled)

    # viz_util.freq(self.input()['order_hour_of_day'].load())

    # agged = util.agg(self.input()['days_since_prior_order'].load(),self.input()['order_id'].load(),['count'])
    # viz_util.bar(agged)

    # print(self.input()['user_id'].load()['user_id'].compute().nunique())

    # agged = util.agg(self.input()['user_id'].load(),self.input()['days_since_prior_order'].load(),['mean'])
    # viz_util.hist(agged,30,0,33)

    ######################## merge

    # grpd = util.groupby(self.input()['orders'],['user_id','order_number'],'user_id',lambda x: x['order_number'].max().value_counts().to_frame())
    # viz_util.bar(grpd,'order_number',merged=True)

    products = util.merge(self.input()['prior'],self.input()['products'],'product_id',['product_id','department_id'],'product_id')
    products = util.merge(products,self.input()['departments'],None,None,'department_id',merged=[True,False])
    # basic_info_df(products)
    viz_util.pie(products,'department',merged=True)

